﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
	public partial class MainPage : ContentPage
	{
        int savedValue1 = 0, savedValue2 = 0, state = 0, op = 0;

		public MainPage()
		{
			InitializeComponent();
		}

        void OnButtonClickedNum(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (MainLabel.Text == "0" || state == 1)
            {
                MainLabel.Text = button.Text;
                state = 0;
            }
            else
                MainLabel.Text = MainLabel.Text + button.Text;
        }

        void OnButtonClickedC(object sender, EventArgs e)
        {
            MainLabel.Text = "0";
            savedValue1 = 0;
        }

        void OnButtonClickedPlus(object sender, EventArgs e)
        {
            op = 0;
            state = 1;
            savedValue1 = Convert.ToInt32(MainLabel.Text);
        }

        void OnButtonClickedMin(object sender, EventArgs e)
        {
            op = 1;
            state = 1;
            savedValue1 = Convert.ToInt32(MainLabel.Text);
        }

        void OnButtonClickedMult(object sender, EventArgs e)
        {
            op = 2;
            state = 1;
            savedValue1 = Convert.ToInt32(MainLabel.Text);
        }

        void OnButtonClickedDiv(object sender, EventArgs e)
        {
            op = 3;
            state = 1;
            savedValue1 = Convert.ToInt32(MainLabel.Text);
        }

        void OnButtonClickedEquals(object sender, EventArgs e)
        {
            int newValue = Convert.ToInt32(MainLabel.Text);
            if (op == 0)
                MainLabel.Text = (savedValue1 + newValue).ToString();
            else if (op == 1)
                MainLabel.Text = (savedValue1 - newValue).ToString();
            else if (op == 2)
                MainLabel.Text = (savedValue1 * newValue).ToString();
            else if (op == 3)
                MainLabel.Text = (savedValue1 / newValue).ToString();

            state = 1;
            newValue = Convert.ToInt32(MainLabel.Text);
            savedValue1 = newValue;
        }
    }
}
